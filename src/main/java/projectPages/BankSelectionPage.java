package projectPages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjMethodsBankBazaar;

public class BankSelectionPage extends ProjMethodsBankBazaar {

	public ContactDetailsPage selectBank(String bank)
	{
		WebElement eleBank = locateElement("xpath","//span[text()='"+bank+"']");
		click(eleBank);
		return new ContactDetailsPage();
	}
}
