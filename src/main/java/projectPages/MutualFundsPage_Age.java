package projectPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjMethodsBankBazaar;

public class MutualFundsPage_Age extends ProjMethodsBankBazaar{

	public MutualFundsPage_Age ageFinder(String age)
	{
		Actions action = new Actions(driver);
		int ageI = Integer.parseInt(age);
		WebElement slider = locateElement("xpath","//div[@class='rangeslider__handle']" );
		//WebElement slider = driver.findElementByXPath("//div[@class='rangeslider__handle']");
		//WebElement ageSlider = locateElement("xpath","//div[@class='rangeslider rangeslider-horizontal']");
		//click(slider);
		int x=(ageI-18)*8;
		action.dragAndDropBy(slider,x, 0).perform();
		//click(slider);
		return this;
	}
	public MutualFundsPage_Age selectMonthYear(String yearmonth)
	{
		WebElement eleMonthYear = locateElement("xpath","//a[contains(text(),'"+yearmonth+"')]");
		click(eleMonthYear);
		return this;
	}

	public MutualFundsPage_Age selectDate(String date)
	{
		WebElement eleDate = locateElement("xpath","//div[contains(text(),'"+date+"')]");
		click(eleDate);
		return this;
	}

	public MutualFundsPage_Salary selectContinue()
	{
		WebElement eleContinueButton = locateElement("LinkText","Continue");
		click(eleContinueButton);
		return new MutualFundsPage_Salary();
	}

}
