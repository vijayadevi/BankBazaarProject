package projectPages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjMethodsBankBazaar;

public class MutualFundsPage_Salary extends ProjMethodsBankBazaar {

	public MutualFundsPage_Salary enterIncome(String income) {
	WebElement eleIncome = locateElement("name","netAnnualIncome");
	type(eleIncome,income);
	return this;
	}
	
	public BankSelectionPage clickContinue()
	{
		WebElement eleContinue = locateElement("linktext","Continue");	
		click(eleContinue);
		return new BankSelectionPage();
	}
}
