package projectPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjMethodsBankBazaar;


public class BankBazaarHomePage extends ProjMethodsBankBazaar{
	
	public BankBazaarHomePage selectInvestment() {
		WebElement eleInvestments = locateElement("linktext", "INVESTMENTS");
        Actions action = new Actions(driver);
        
        action.moveToElement(eleInvestments).build().perform();
        return this; 
	}
	
	public MutualFundPage selectMutualFund() throws InterruptedException {
        WebElement eleMutualFunds = locateElement("linktext", "Mutual Funds");
        click(eleMutualFunds);
        Thread.sleep(5000);
        return new MutualFundPage();
	}



	}
	
	

