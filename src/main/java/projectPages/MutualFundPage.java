package projectPages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjMethodsBankBazaar;

public class MutualFundPage extends ProjMethodsBankBazaar{
	
	public MutualFundsPage_Age searchMutualFunds() {
		
		 WebElement elesearchMutual = locateElement("linktext", "Search for Mutual Funds");
		 click(elesearchMutual);
		 return new MutualFundsPage_Age();
	}

}
