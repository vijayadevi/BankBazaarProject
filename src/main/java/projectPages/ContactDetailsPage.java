package projectPages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjMethodsBankBazaar;

public class ContactDetailsPage extends ProjMethodsBankBazaar{
	
	public ContactDetailsPage enterContactName(String name) {
		WebElement eleFirstName = locateElement("name","firstName");
		type(eleFirstName,name);
		return this;
	}

	public SchemeListPage clickViewMutualFunds()
	{
		WebElement eleViewMutualFUnds = locateElement("linktext","View Mutual Funds");
		click(eleViewMutualFUnds);
		return new SchemeListPage();
	}
}
