package projectPages;

import java.util.List;

import org.openqa.selenium.WebElement;

import wdMethods.ProjMethodsBankBazaar;

public class SchemeListPage extends ProjMethodsBankBazaar{

	public void getAllSchemes() {
		List<WebElement> allSchemes = driver.findElementsByClassName("js-offer-name");
		for (WebElement eachScheme : allSchemes) {
			System.out.println(eachScheme.getText());
			WebElement eleAmount = driver.findElementByXPath("//span[contains(text(),'"+eachScheme.getText()+"')]/following::span[@class='fui-rupee bb-rupee-xs']/..");
			System.out.println(eleAmount.getText());
	}
	}
}
	
