package projectTestCases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import projectPages.BankBazaarHomePage;
import wdMethods.ProjMethodsBankBazaar;

public class BankBazaarTC001 extends ProjMethodsBankBazaar{
	

	@BeforeClass
	public void setData() {
		testCaseName = "TC001_BankBazaar";
		testCaseDescription ="GetSchemes";
		category = "Smoke";
		author= "Viji";
		dataSheetName="BankBazaar_TC01";
	}
	@Test(dataProvider="fetchData")
	public  void getScheme(String age, String yearmonth, String date, String income, String bank, String contactname) throws InterruptedException   {
		new BankBazaarHomePage()
		.selectInvestment()
		.selectMutualFund()
		.searchMutualFunds()
		.ageFinder(age)
		.selectMonthYear(yearmonth)
		.selectDate(date)
		.selectContinue()
		.enterIncome(income)
		.clickContinue()
		.selectBank(bank)
		.enterContactName(contactname)
		.clickViewMutualFunds()
		.getAllSchemes();
			
	}
}
