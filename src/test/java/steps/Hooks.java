package steps;

import org.openqa.selenium.WebElement;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods {
	@Before
	public void beforeCucumber(Scenario sc) {
		startResult();
		testCaseName = sc.getName();
		testCaseDescription = sc.getName();
		category = "Smoke";
		author = "Babu";
		startTestCase();
		login("http://leaftaps.com/opentaps", "DemoSalesManager", "crmsfa");

	}

	@After
	public void afterScenario(Scenario sc) {
		closeAllBrowsers();
		stopResult();
		System.out.println("Status: " + sc.getStatus());
	}

	public void login(String url, String userName, String passWord) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, userName);
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, passWord);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
		click(eleCRM);
	}
}
