package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {/*

	public static RemoteWebDriver driver;

	@Given("launch the browser")
	public void launchBrowser()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@And("maximize the browser")
	public void maxBrowser()
	{
		driver.manage().window().maximize();
	}

	@And("set the timeouts")
	public void setTimeOut()
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@And("enter the URL")
	public void enterUrl()
	{
		driver.get("http://leaftaps.com/opentaps");
	}


	@And("enter the user name as (.*)")
	public void enterusername (String uname)
	{
		driver.findElementById("username").sendKeys(uname);
	}

	@And("enter the password as (.*)")
	public void enterpassword(String pwd)
	{
		driver.findElementById("password").sendKeys(pwd);
	}

	@And("clicks on the login button")
	public void clickLogin()
	{
		driver.findElementByClassName("decorativeSubmit").click();
	}


	@And ("click on CRMSFA")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@And ("click on create lead")

	public void clickcreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		// Fill in the Create Lead form
		// Text Box
	}
	@And ("enter company name as (.*)") 
	public void enterData(String cname) {

		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}

	@And ("enter firstname as (.*)")

	public void enterFirstname(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);

	}

	@And ("enter lastname as (.*)")
	public void enterLastname(String lname) {

		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}




	@When ("clicks on Create Lead button")
	// Create Lead
	public void clickCreateLead() throws Throwable {
		driver.findElementByClassName("smallSubmit").click();
		Thread.sleep(3000);

	}
	@Then ("View Lead page appears")
	public void verifyLead() {

		System.out.println("Lead Created Successfully..!!");
		driver.close();

	}


*/}
