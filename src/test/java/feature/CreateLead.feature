Feature: Create Lead in LeafTap application

Scenario Outline: Create Lead Scenario
And click on create lead
And enter company name as <companyname> 
And enter firstname as <firstname>
And enter lastname as <lastname>
When clicks on Create Lead button
Then View Lead page appears
Examples:
|companyname|firstname|lastname|
|CTS|AAAA|LLLL|
|TCS|BBBB|NNNN|

