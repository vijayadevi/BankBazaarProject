Feature: Login into LeafTap application
Scenario: Positive flow for Login
Given launch the browser
And maximize the browser
And set the timeouts
And enter the URL
And enter the user name as DemoSalesManager
And enter the password as crmsfa
When clicks on the login button
Then verify login is success